![logo](https://user-images.githubusercontent.com/29823650/127615109-0306517b-554d-402f-bd1b-c667b2b5d21d.png)

# Amuse Me
> Application made for you!


## Project setup

```sh
Clone the repo, open the project in Android Studio, hit "Run". Done!
```

## Introduction

In everyday life we work, study, or do something else that keeps us busy and when the time comes we need to rest and have some fun. In those cases there come thoughts, such as: "I don't know where to go", "I would like to go somewhere but everyone's busy",
"I want to meet new people", etc. So we provide the Whole New World to you. 
All you have to do is:

![dicedice](https://user-images.githubusercontent.com/29823650/127615885-22249902-87d7-4df0-bdc8-bc50db1759ff.PNG)
- Open our app
- Register/Login
- Fill questionnaire
- Chat with new people, follow directions
- Have fun ('cause we all deserve it)

## Used libraries and technologies
- Dager Hilt
- Coroutines
- Retrofit
- Livedata
- Navigation
- Google play services
- FireBase realtime database
- Firebase authorization
- Firebase Cloud Messaging
- Glide

## Platform and language
- Android Studio
- Kotlin

## Architecture
- MVVM pattern

## Feedback 
Any questions or suggestions?
