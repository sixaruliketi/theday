package com.example.theday.model

data class Error(val error: ErrorType) {
    data class ErrorType(val code: Int, val message: String)
}