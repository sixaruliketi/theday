package com.example.theday.model

data class Place(
    val description: String? = null,
    val lat: Double? = null,
    val long: Double? = null,
    val name: String? = null,
    val pic1: String? = null,
    val pic2: String? = null,
    val pic3: String? = null,
    val pic4: String? = null,
    val rating: Long? = null,
    val ready: Long? = null)