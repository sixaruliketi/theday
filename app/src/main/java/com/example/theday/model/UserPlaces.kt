package com.example.theday.model

data class UserPlaces(
    val placeName: String? = null,
    val placePic: String? = null,
    val placeRating: Double? = null,
)