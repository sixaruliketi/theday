package com.example.theday.model

data class Message(
    val message: String? = null,
    val sender: String? = null,
    val userName: String? = null,
    val senderPic: String? = null
)