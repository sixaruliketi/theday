package com.example.theday.model

data class Questionnaire(
    val question: String? = null,
    val answerTxt1: String? = null,
    val answerTxt2: String? = null,
    val answerTxt3: String? = null,
    val answerTxt4: String? = null)