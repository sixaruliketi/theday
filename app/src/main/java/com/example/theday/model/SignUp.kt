package com.example.theday.model

data class SignUp(
    val email: String?,
    val localId: String?)
