package com.example.theday.model

data class UserInfo(
    val email: String? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val username: String? = null,
    val profilePic: String? = null,
    val activeEvent: String? = null,
    val bio: String? = null,
    val navigated: String? = null,
    val activeEventName: String? = null
//                    val userBio: String? = null,
)


