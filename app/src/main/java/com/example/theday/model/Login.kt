package com.example.theday.model

data class Login(
    val email: String?,
    val localId: String?,
    val registered: Boolean
)
