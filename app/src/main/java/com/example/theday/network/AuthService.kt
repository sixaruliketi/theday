package com.example.theday.network

import com.example.theday.model.Login
import com.example.theday.model.SignUp
import com.example.theday.model.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface AuthService {

    @POST("v1/accounts:signInWithPassword?")
    suspend fun logIn(
        @Body user: User,
        @Query("key")
        apiKey: String = "AIzaSyC7YtT-LNl03KNjl9Mno0_2kx-9tNPK1WY"
    ): Response<Login>

    @POST("v1/accounts:signUp?")
    suspend fun signUp(
        @Body user: User,
        @Query("key")
        apiKey: String = "AIzaSyC7YtT-LNl03KNjl9Mno0_2kx-9tNPK1WY"
    ): Response<SignUp>

}