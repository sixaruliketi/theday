package com.example.theday.extencions

fun String.isEmail() = android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()