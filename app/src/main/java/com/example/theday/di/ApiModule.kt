package com.example.theday.di

import com.example.theday.network.AuthService
import com.example.theday.repository.AuthRepository
import com.example.theday.repository.AuthRepositoryImplement
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    companion object {

        const val BASE_URL = "https://identitytoolkit.googleapis.com/"

    }

    @Provides
    @Singleton
    fun login() = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(AuthService::class.java)

    @Provides
    @Singleton
    fun provideLoginRepostory(
        authService: AuthService,
    ): AuthRepository = AuthRepositoryImplement(authService)

}