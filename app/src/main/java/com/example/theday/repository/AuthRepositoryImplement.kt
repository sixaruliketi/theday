package com.example.theday.repository

import com.example.theday.model.Error
import com.example.theday.model.Login
import com.example.theday.model.SignUp
import com.example.theday.model.User
import com.example.theday.network.AuthService
import com.example.theday.network.Resource
import com.google.gson.Gson
import javax.inject.Inject

class AuthRepositoryImplement @Inject constructor(
    private val authService: AuthService
) : AuthRepository {

    override suspend fun logIn(
        email: String,
        password: String,
    ): Resource<Login> {
        return try {
            val response = authService.logIn(User(email, password))
            if (response.isSuccessful) {

                val body = response.body()!!

                Resource.succsess(body)

            } else {
                val errorModel =
                    Gson().fromJson(response.errorBody()!!.string(), Error::class.java)
                Resource.error(errorModel.error.message)

            }
        } catch (e: Exception) {

            Resource.error(e.message.toString())

        }
    }


    override suspend fun signUp(
        email: String,
        password: String,
    ): Resource<SignUp> {
        return try {
            val response = authService.signUp(User(email, password))
            if (response.isSuccessful) {

                val body = response.body()!!

                Resource.succsess(body)

            } else {
                val errorModel =
                    Gson().fromJson(response.errorBody()!!.string(), Error::class.java)
                Resource.error(errorModel.error.message)

            }
        } catch (e: Exception) {

            Resource.error(e.message.toString())

        }
    }
}