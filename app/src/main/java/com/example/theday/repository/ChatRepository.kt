package com.example.theday.repository

import com.example.theday.model.Message
import com.example.theday.network.Resource
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import javax.inject.Inject

typealias getMessages = (Resource<MutableList<Message>>) -> Unit

class ChatRepository @Inject constructor() {

    fun sentMessage(chatName: String, message: Message) {
        FirebaseDatabase.getInstance().getReference("Messages").child(chatName).push().setValue(message)
    }

    private var messages = mutableListOf<Message>()

    fun getMessages(chatName: String,getMessages:getMessages) {

        FirebaseDatabase.getInstance().getReference("Messages").child(chatName)
        .addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                messages.add(snapshot.getValue(Message::class.java)!!)
                getMessages(Resource.succsess(messages))

            }

            override fun onChildChanged(
                snapshot: DataSnapshot,
                previousChildName: String?
            ) {

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onCancelled(error: DatabaseError) {
                getMessages(Resource.error(error.message))
            }

        })
    }
}