package com.example.theday.repository

import com.example.theday.model.Place
import com.example.theday.network.Resource
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import dagger.Provides
import javax.inject.Inject

typealias place = (Resource<Place>) -> Unit

class GoogleMapRepository @Inject constructor() {


    fun getPlace(placeName: String,place:place) {

        FirebaseDatabase.getInstance().getReference("places").child(placeName)
            .addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                place(Resource.succsess(snapshot.getValue(Place::class.java)!!))

            }

            override fun onCancelled(error: DatabaseError) {
                place(Resource.error(error.message))

            }
        })
    }

    fun ready(placeName: String, cout: Int){

        FirebaseDatabase.getInstance().getReference("places").child(placeName)
            .child("ready").setValue(cout)

    }

    fun navigated(uId: String) {
        FirebaseDatabase.getInstance().getReference("User").child(uId).child("navigated")
            .setValue("yes")
    }

    fun activeEvent(uId: String){
        FirebaseDatabase.getInstance().getReference("User").child(uId).child("activeEvent")
            .setValue("yes")
    }

    fun setEventName(uId: String, eventName: String) {
        FirebaseDatabase.getInstance().getReference("User").child(uId).child("activeEventName")
            .setValue(eventName)
    }

}