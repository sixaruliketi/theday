package com.example.theday.repository

import com.example.theday.model.Login
import com.example.theday.model.SignUp
import com.example.theday.network.Resource

interface AuthRepository {

    suspend fun logIn(email: String, password: String): Resource<Login>

    suspend fun signUp(email: String, password: String): Resource<SignUp>

}