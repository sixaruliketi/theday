package com.example.theday.repository

import com.example.theday.model.UserPlaces
import com.google.firebase.database.FirebaseDatabase
import javax.inject.Inject

class RatingRepository @Inject constructor() {

    fun addUserHistory(uid:String,placeName:String,place: UserPlaces){
        FirebaseDatabase.getInstance().getReference("User").child(uid).child("places").child(placeName).setValue(place)
    }

    fun deactivateEvent(uid:String){
        FirebaseDatabase.getInstance().getReference("User").child(uid).child("activeEvent").setValue("no")
    }

    fun deactivateNavigate(uid:String){
        FirebaseDatabase.getInstance().getReference("User").child(uid).child("navigated").setValue("no")
    }
}