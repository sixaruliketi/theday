package com.example.theday.repository

import com.example.theday.model.Questionnaire
import com.example.theday.model.UserInfo
import com.example.theday.network.Resource
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import javax.inject.Inject

typealias questions = (Resource<MutableList<Questionnaire>>) -> Unit

class QuestionnaireRepository@Inject constructor() {

    private var questionnaire = mutableListOf<Questionnaire>()

    fun getItem(questions:questions) {

        FirebaseDatabase.getInstance().getReference("Questionnaire")
            .addChildEventListener(object :
            ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                questionnaire.add(snapshot.getValue(Questionnaire::class.java)!!)
                questions(Resource.succsess(questionnaire))

            }

            override fun onChildChanged(
                snapshot: DataSnapshot,
                previousChildName: String?
            ) {

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onCancelled(error: DatabaseError) {
                questions(Resource.error(error.message))
            }
        })
    }
}