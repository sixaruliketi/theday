package com.example.theday.repository

import com.example.theday.model.UserInfo
import com.example.theday.model.UserPlaces
import com.example.theday.network.Resource
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import javax.inject.Inject

typealias userInfo = (Resource<UserInfo>) -> Unit

typealias placesHistory = (Resource<MutableList<UserPlaces>>) -> Unit

class HomeRepository@Inject constructor()  {

     fun getUserInfo(uId: String,userInfo : userInfo) {

        FirebaseDatabase.getInstance().getReference("User").child(uId).get().addOnCompleteListener {

               if (it.isSuccessful){
                   userInfo (Resource.succsess(it.result!!.getValue(UserInfo::class.java)!!))
               }
               else{

                   userInfo (Resource.error(it.exception?.message))
               }

           }.addOnFailureListener {

            userInfo (Resource.error(it.message))

        }
    }



    fun getUserPlaceHistory(uId: String,placesHistory : placesHistory) {

        var placeHistory = mutableListOf<UserPlaces>()

        FirebaseDatabase.getInstance().getReference("User").child(uId).child("places")
        .addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                snapshot.getValue(UserPlaces::class.java)?.let { placeHistory.add(it) }
                placesHistory(Resource.succsess(placeHistory))
            }

            override fun onChildChanged(
                snapshot: DataSnapshot,
                previousChildName: String?
            ) {

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onCancelled(error: DatabaseError) {
                placesHistory(Resource.error(error.message))
            }
        })
    }
}