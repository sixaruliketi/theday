package com.example.theday.ui.auth.login

import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.theday.BaseFragment
import com.example.theday.R
import com.example.theday.databinding.LogInFragmentBinding
import com.example.theday.extencions.isEmail
import com.example.theday.extencions.setUp
import com.example.theday.network.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LogInFragment : BaseFragment<LogInFragmentBinding, LogInViewModel>(
    LogInFragmentBinding::inflate,
    LogInViewModel::class.java
) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        listeners()
        observe()
    }

    private fun listeners() {

        binding!!.logInBtn.setOnClickListener {
            binding!!.progressBar.visibility = View.VISIBLE
            logIn()

        }

        binding!!.dontHaveAccBtn.setOnClickListener {
            findNavController().navigate(R.id.action_logInFragment_to_signUpFragment)
        }

    }

    private fun logIn() {

        val email = binding!!.emailET.text.toString().trim()
        val password = binding!!.passwordET.text.toString().trim()

        if (email.isEmpty()) {


            showErrorDialog("Something went wrong", "Email is required")
            return

        }

        if (!email.isEmail()) {


            showErrorDialog("Something went wrong", "Email is incorrect")
            return

        }

        if (password.isEmpty()) {


            showErrorDialog("Something went wrong", "Password is required")
            return

        }

        viewModel.logIn(email, password)

    }


    private fun observe() {

        viewModel._logInLiveData.observe(viewLifecycleOwner, {

            when (it.status) {

                Resource.Status.Succsess -> {

                    //shemowmeba gavliliaq tu ara intro da mere navigacia
                    val uId = bundleOf("uId" to it.data!!.localId.toString())


                    binding!!.progressBar.visibility = View.INVISIBLE
                    findNavController().navigate(R.id.action_logInFragment_to_homeFragment, uId)
                }
                Resource.Status.Error -> {
                    showErrorDialog("Something went wrong", it.message.toString())
                }

                else -> {
                }
            }

        })

    }

    private fun showErrorDialog(title: String, description: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description
        dialog.findViewById<Button>(R.id.continuee).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }


}