package com.example.theday.ui.home

import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.theday.BaseFragment
import com.example.theday.R
import com.example.theday.databinding.HomeFragmentBinding
import com.example.theday.extencions.setUp
import com.example.theday.network.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<HomeFragmentBinding, HomeViewModel>(
    HomeFragmentBinding::inflate,
    HomeViewModel::class.java
) {

    private lateinit var adapter: HistoryRecyclerViewAdapter
    private lateinit var drawerHeaderAdapter: HeaderRecyclerAdapter
    private var uId = ""
    private var activeEvent: String? = ""
    private var activeEventName: String? = ""
    private var userName = ""
    private var userPic = ""
    private var navigated = ""

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        uId = arguments?.getString("uId").toString()
        viewModel.getUserInfo(uId)
        viewModel.getUserPlaceHistory(uId)
        init()
    }

    private fun init() {

        observes()
        listeners()
        drawerLayout()

    }

    private fun drawerLayout() {
        adapter = HistoryRecyclerViewAdapter()
        binding!!.HistoryRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding!!.HistoryRecyclerView.adapter = adapter

        initDrawerHeader()

    }

    private fun initDrawerHeader() {

        drawerHeaderAdapter = HeaderRecyclerAdapter()
        binding!!.headerRecycler.layoutManager = LinearLayoutManager(requireContext())
        binding!!.headerRecycler.adapter = drawerHeaderAdapter

        drawerHeaderAdapter.click = {
            if(it){
                findNavController().navigateUp()
            }
        }
    }

    private fun listeners() {
        binding!!.HomeStartButton.setOnClickListener {
            binding!!.progressBar.visibility = View.VISIBLE

            if (activeEvent == "yes") {

                if (navigated == "yes"){

                    val userInfo = bundleOf(
                        "uId" to uId,
                        "placeName" to activeEventName,
                    )
                    findNavController().navigate(
                        R.id.action_homeFragment_to_raiting,
                        userInfo
                    )

                }else{

                    val userInfo = bundleOf(
                        "uId" to uId,
                        "placeName" to activeEventName,
                        "userName" to userName,
                        "userPic" to userPic,
                    )
                    binding!!.progressBar.visibility = View.INVISIBLE
                    findNavController().navigate(
                        R.id.action_homeFragment_to_googleMapFragment,
                        userInfo
                    )

                }


            } else {

                val userInfo = bundleOf(
                    "uId" to uId,
                    "userName" to userName,
                    "userPic" to userPic
                )

                findNavController().navigate(R.id.action_homeFragment_to_questionnaireFragment, userInfo)
            }


        }

    }

    private fun observes() {

        viewModel._userInfo.observe(viewLifecycleOwner, {

            when (it.status){
                Resource.Status.Succsess ->{
                    userName = it.data!!.username.toString()
                    userPic = it.data.profilePic.toString()
                    activeEvent = it.data.activeEvent.toString()
                    activeEventName = it.data.activeEventName.toString()
                    navigated = it.data.navigated.toString()

                    adapter.getUserInfo(it.data)

                    drawerHeaderAdapter.getUserInfo(it.data)}
                Resource.Status.Error -> {

                    showErrorDialog("Something went wrong",it.message.toString())

                }
                else -> {}
            }


        })

        viewModel._userPlacesLivedata.observe(viewLifecycleOwner, {

            when (it.status){
                Resource.Status.Succsess ->{
                    adapter.getUserPlaceHistory(it.data!!)
                }
                Resource.Status.Error ->{
                    showErrorDialog("Something went wrong",it.message.toString())
                }
                else->{}
            }


        })


    }

    private fun showErrorDialog(title: String, description: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description
        dialog.findViewById<Button>(R.id.continuee).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }


}