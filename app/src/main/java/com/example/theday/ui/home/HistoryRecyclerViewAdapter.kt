package com.example.theday.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.theday.databinding.ProfileHistoryItemBinding
import com.example.theday.model.UserInfo
import com.example.theday.model.UserPlaces

class HistoryRecyclerViewAdapter: RecyclerView.Adapter<HistoryRecyclerViewAdapter.ViewHolder>() {

    private lateinit var userInfo :UserInfo

    private   var  placeHistory = mutableListOf<UserPlaces>()

    inner class ViewHolder(private val binding: ProfileHistoryItemBinding) : RecyclerView.ViewHolder(binding.root){

        private lateinit var UserPlaces: UserPlaces



        fun bind(){
            UserPlaces = placeHistory[absoluteAdapterPosition]

            binding.placeRating.rating = UserPlaces.placeRating!!.toFloat()

            binding.userPlace.text = UserPlaces.placeName.toString()
            Glide.with(binding.placePic.context).load(UserPlaces.placePic).into(binding.placePic)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        ProfileHistoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }


     fun getUserInfo( userInfo: UserInfo){

        this.userInfo = userInfo

        notifyDataSetChanged()

    }

     fun getUserPlaceHistory(places: List<UserPlaces>){
        this.placeHistory = places.toMutableList()
         notifyDataSetChanged()
    }

    override fun getItemCount(): Int = placeHistory.size
}