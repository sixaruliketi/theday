package com.example.theday.ui.intro

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.example.theday.R


class LayoutPagerAdapter(val layoutInflater: LayoutInflater) : PagerAdapter() {

    val layouts: Array<Int> = arrayOf(
        R.layout.intro_fragment, R.layout.intro_fragment_2,
        R.layout.intro_fragment_3, R.layout.intro_fragment_4, R.layout.intro_fragment_final
    )

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }

    override fun getCount(): Int {
        return layouts.size
    }

    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        parent.removeView(`object` as View)
    }

    override fun instantiateItem(viewGroup: ViewGroup, position: Int): Any =
        LayoutInflater.from(viewGroup.context).inflate(layouts[position], viewGroup, false).also {
            viewGroup.addView(it)

        }
}