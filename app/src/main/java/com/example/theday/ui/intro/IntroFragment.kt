package com.example.theday.ui.intro

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.example.theday.BaseFragment
import com.example.theday.R
import com.example.theday.databinding.IntroFeatureFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class IntroFragment : BaseFragment<IntroFeatureFragmentBinding, IntroViewModel>(
    IntroFeatureFragmentBinding::inflate, IntroViewModel::class.java
) {

    val sharedPreference = context?.getSharedPreferences("user", Context.MODE_PRIVATE)

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        init()
    }

    private fun init() {

        binding!!.viewPager.adapter = LayoutPagerAdapter(layoutInflater)

        binding!!.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position == 4) {
                    navigate()
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })


    }

    private fun navigate() {

        val btn = requireView().findViewById<Button>(R.id.lastIntroBtn)
        btn.setOnClickListener {
            viewModel.saveIntroStatus()
            findNavController().navigate(R.id.action_introFragment_to_logInFragment)

        }

    }
}