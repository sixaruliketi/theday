package com.example.theday.ui.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.theday.R
import com.example.theday.databinding.RecivedMessageBinding
import com.example.theday.databinding.SentMessageBinding
import com.example.theday.model.Message

class ChatRecyclerAdapter(val uid: String?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val sentViewType = 1
        private const val recivedViewType = 2
    }

    private var messages = mutableListOf<Message>()

    inner class SentMessageViewHolder(private val binding: SentMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var model: Message

        fun bind() {

            model = messages[adapterPosition]
            binding.message.text = model.message
            binding.userName.text = model.userName

            Glide.with(binding.root.context).load(model.senderPic).placeholder(R.drawable.profile_pic).circleCrop()
                .into(binding.userImg)

        }

    }

    inner class RecivedMessageViewHolder(private val binding: RecivedMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var model: Message

        fun bind() {

            model = messages[adapterPosition]
            binding.message.text = model.message
            binding.userName.text = model.userName

            Glide.with(binding.root.context).load(model.senderPic).circleCrop()
                .into(binding.userImg)
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].sender == uid) {
            sentViewType
        } else {
            recivedViewType
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == sentViewType) {
            SentMessageViewHolder(
                SentMessageBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            RecivedMessageViewHolder(
                RecivedMessageBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SentMessageViewHolder) {
            holder.bind()
        } else if (holder is RecivedMessageViewHolder) {
            holder.bind()
        }
    }

    override fun getItemCount() = messages.size

    fun getMessages(messages: List<Message>) {

        this.messages = messages as MutableList<Message>
        notifyDataSetChanged()

    }
}