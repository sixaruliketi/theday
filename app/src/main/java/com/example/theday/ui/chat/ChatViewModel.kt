package com.example.theday.ui.chat

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.theday.model.Message
import com.example.theday.network.Resource
import com.example.theday.repository.ChatRepository
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ChatViewModel @Inject constructor(private val chatRepo : ChatRepository) : ViewModel() {

    private var getMessagesLivedata = MutableLiveData<Resource<MutableList<Message>>>()

    var _getMessagesLivedata: LiveData<Resource<MutableList<Message>>> = getMessagesLivedata

    fun sentMessage(chatName: String, message: Message) {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                chatRepo.sentMessage(chatName,message)
            }
        }
    }

    fun getMessages(chatName: String) {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                chatRepo.getMessages(chatName){
                    getMessagesLivedata.postValue(it)
                }
            }
        }
    }
}