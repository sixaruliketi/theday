package com.example.theday.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.theday.R
import com.example.theday.databinding.ProfileHeaderBinding
import com.example.theday.model.UserInfo

typealias click = (position : Boolean) -> Unit

class HeaderRecyclerAdapter() : RecyclerView.Adapter<HeaderRecyclerAdapter.ViewHolder>() {

    private var userInfo: UserInfo? = null
     lateinit var  click : click


    inner class ViewHolder(private val binding: ProfileHeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind() {

            if (userInfo != null) {

                binding.userEmail.text = userInfo!!.email
                binding.userUsername.text = userInfo!!.username
                binding.userBio.text = userInfo!!.bio
                binding.logOutBtn.setOnClickListener {
                    click.invoke(true)
                }

                Glide.with(binding.root.context)
                    .load(userInfo!!.profilePic)
                    .placeholder(R.drawable.profile_pic)
                    .circleCrop()
                    .into(binding.userAvatar)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ProfileHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }


    fun getUserInfo(userInfo: UserInfo) {

        this.userInfo = userInfo

        notifyDataSetChanged()

    }

    override fun getItemCount(): Int = 1
}