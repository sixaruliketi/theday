package com.example.theday.ui.auth.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.theday.model.Login
import com.example.theday.network.Resource
import com.example.theday.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class LogInViewModel @Inject constructor(val authRepository: AuthRepository) : ViewModel() {

    private var logInLiveData = MutableLiveData<Resource<Login>>()

    var _logInLiveData: LiveData<Resource<Login>> = logInLiveData

    fun logIn(email: String, password: String) {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val result = authRepository.logIn(email, password)
                logInLiveData.postValue(result)
            }
        }
    }
}