package com.example.theday.ui.questionnaire

import android.app.Dialog
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.theday.BaseFragment
import com.example.theday.R
import com.example.theday.databinding.FragmentQuestionnaireBinding
import com.example.theday.extencions.setUp
import com.example.theday.network.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QuestionnaireFragment : BaseFragment<FragmentQuestionnaireBinding, QuestionnaireViewModel>(
    FragmentQuestionnaireBinding::inflate,
    QuestionnaireViewModel::class.java
) {


    private var uId = ""
    private var activeEvent: String? = ""
    private var activeEventName: String? = ""
    private var userName = ""
    private var userPic = ""

    private lateinit var adapter: QuestionnaireRecyclerViewAdapter

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        uId = arguments?.getString("uId").toString()
        userName = arguments?.getString("userName").toString()
        userPic = arguments?.getString("userPic").toString()

        init()
    }

    private fun init() {
        viewModel.getItem()
        observe()
        adapterInit()
    }

    private fun adapterInit() {

        adapter = QuestionnaireRecyclerViewAdapter()
        binding!!.questionnaireRecyclerView.layoutManager = LinearLayoutManager(context)
        binding!!.questionnaireRecyclerView.adapter = adapter

        adapter.selectedItem = {
            if (it) {
                var grade = adapter.getUserGrade()


                when (grade) {
                    in 10..14 -> activeEventName = "Rock Paper Scissors"
                    in 15..19 -> activeEventName = "Métis"
                    in 20..23 -> activeEventName = "MacLaren's Irish Pub"
                    in 24..27 -> activeEventName = "Mtatsminda Park"
                    in 28..35 -> activeEventName = "Tbilisi Digital Space"
                    in 36..40 -> activeEventName = "AirSoft Club"
                 }

                val userInfo = bundleOf(
                    "uId" to uId,
                    "placeName" to activeEventName,
                    "userName" to userName,
                    "userPic" to userPic
                )

                findNavController().navigate(R.id.action_questionnaireFragment_to_googleMapFragment,userInfo)

            }
        }
    }

    private fun observe() {

        viewModel._getQuestionnaireLivedata.observe(
            viewLifecycleOwner,
            {
                when(it.status){
                    Resource.Status.Succsess -> {
                        adapter.getData(it.data!!)
                    }
                    Resource.Status.Error -> {
                        showErrorDialog("Something went wrong",it.message.toString())
                    }

                    else -> {}
                }


            }
        )

    }

    private fun showErrorDialog(title: String, description: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description
        dialog.findViewById<Button>(R.id.continuee).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }
}