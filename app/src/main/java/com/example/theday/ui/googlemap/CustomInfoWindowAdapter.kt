package com.example.theday.ui.googlemap

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.theday.R
import com.example.theday.model.Place
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class CustomInfoWindowAdapter(val context: Context, val place: Place) :
    GoogleMap.InfoWindowAdapter {

    val windows = LayoutInflater.from(context).inflate(R.layout.custom_info_window, null)

    override fun getInfoWindow(p0: Marker?): View {
        renderWindow(windows)
        return windows
    }

    override fun getInfoContents(p0: Marker?): View {
        renderWindow(windows)
        return windows
    }

    private fun renderWindow(view: View) {

        val title = view.findViewById<TextView>(R.id.cIWATitle)
        val description = view.findViewById<TextView>(R.id.desCIW)
        val img = view.findViewById<ImageView>(R.id.cIWAPic)
        val img1 = view.findViewById<ImageView>(R.id.cIWAPic1)
        val img2 = view.findViewById<ImageView>(R.id.cIWAPic2)
        val img3 = view.findViewById<ImageView>(R.id.cIWAPic3)

        description.text = place.description

        title.text = place.name

        Glide.with(context)
            .load(place.pic1)
            .placeholder(R.drawable.dialog_background)
            .into(img)

        Glide.with(context)
            .load(place.pic2)
            .placeholder(R.drawable.dialog_background)
            .into(img1)

        Glide.with(context)
            .load(place.pic3)
            .placeholder(R.drawable.dialog_background)
            .into(img2)

        Glide.with(context)
            .load(place.pic4)
            .placeholder(R.drawable.dialog_background)
            .into(img3)

    }
}