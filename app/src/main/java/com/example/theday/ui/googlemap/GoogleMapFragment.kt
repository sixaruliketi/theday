package com.example.theday.ui.googlemap


import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper.getMainLooper
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.theday.BaseFragment
import com.example.theday.R
import com.example.theday.databinding.GoogleMapFragmentBinding
import com.example.theday.extencions.setUp
import com.example.theday.model.Place
import com.example.theday.network.Resource
import com.example.theday.ui.chat.ChatFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GoogleMapFragment : BaseFragment<GoogleMapFragmentBinding, GoogleMapViewModel>(
    GoogleMapFragmentBinding::inflate,
    GoogleMapViewModel::class.java
) {
    private var placeName = ""
    private var userName = ""
    private var uid = ""
    private var userPic = ""
    private var placePic = ""
    private var navigated = false


    private lateinit var mMap: GoogleMap
    private var mapReady = false
    private var place = Place()
    private var count = 0
    private var readyCount = 0


    val bootomSheetFragment = ChatFragment()

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        uid = arguments?.getString("uId").toString()
        placeName = arguments?.getString("placeName").toString()
        userName = arguments?.getString("userName").toString()
        userPic = arguments?.getString("userPic").toString()

        viewModel.activeEvent(uid)
        viewModel.setEventName(uid, placeName)
        viewModel.getPlace(placeName)

        init()

    }

    private fun init() {
        observes()
        initMap()
        listeners()
    }

    private fun listeners() {

        binding!!.readyBtn.setOnClickListener {

            if (readyCount % 2 == 0) {
                readyCount++
                binding!!.readyBtn.text = "ofc im ready"
                viewModel.ready(placeName, count + 1)

            } else {
                readyCount++
                binding!!.readyBtn.text = "Ready?"
                viewModel.ready(placeName, count - 1)
            }
        }

        binding!!.chatBtn.setOnClickListener {

            val bundle = Bundle()
            bundle.putString("uid", uid)
            bundle.putString("userName", userName)
            bundle.putString("placeName", placeName)
            bundle.putString("userPic", userPic)
            bootomSheetFragment.arguments = bundle
            bootomSheetFragment.show(childFragmentManager, "ChatFragment")
        }
    }

    private fun initMap() {

        val mapFragment =
            childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync { googleMap ->
            mMap = googleMap
            mapReady = true
            updateMap()
        }
    }


    private fun updateMap() {

        if (mapReady && place.lat != null && place.long != null) {

            val placeMarker = LatLng(place.lat!!.toDouble(), place.long!!.toDouble())
            mMap.addMarker(MarkerOptions().position(placeMarker).title(place.name))
            mMap.setInfoWindowAdapter(CustomInfoWindowAdapter(requireContext(), place))
            mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        place.lat!!.toDouble(),
                        place.long!!.toDouble()
                    ), 13f
                )
            )
            mMap.setMaxZoomPreference(20f)
            mMap.setMinZoomPreference(10f)

            mMap.setOnMarkerClickListener {

                it.showInfoWindow()

                Handler(getMainLooper()).postDelayed({
                    it.hideInfoWindow()
                }, 99)

                Handler(getMainLooper()).postDelayed({
                    it.showInfoWindow()
                }, 100)

                true
            }
        }
    }


    private fun observes() {

        viewModel._placeLivedata.observe(viewLifecycleOwner, {

            when(it.status){

                Resource.Status.Succsess -> {
                    place = it.data!!
                    placePic = it.data.pic1.toString()
                    updateMap()
                    count = it.data.ready?.toInt() ?: 0

                    changeReadyBtnTxt()

                    if (it.data.ready!!.toInt() >= 10) {

                        showNavigationDialog(
                            "Start jurney", "10 member is ready lets go start navigation \n " +
                                    "press navigate button"
                        )

                    }}

                Resource.Status.Error ->{
                    showErrorDialog("Something went wrong",it.message.toString())
                }
                else -> {}
            }
        })
    }

    private fun changeReadyBtnTxt() {

        if (place.ready!!.toInt() >= 10) {
            binding!!.readyBtn.isClickable = false
            binding!!.readyBtn.text = "ofc ready"
        }

    }

    private fun showNavigationDialog(title: String, description: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description
        dialog.findViewById<Button>(R.id.continuee).text = "navigate"
        dialog.findViewById<Button>(R.id.continuee).setOnClickListener {
            viewModel.navigated(uid)
            ifNavigated()
            val intent: Intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q=${place.lat},${place.long}&mode=d")
            )
            intent.setPackage("com.google.android.apps.maps")
            startActivity(intent)
            dialog.cancel()
        }
        dialog.show()
    }
    private fun showErrorDialog(title: String, description: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description
        dialog.findViewById<Button>(R.id.continuee).text = "Close"
        dialog.findViewById<Button>(R.id.continuee).setOnClickListener {
            viewModel.navigated(uid)
            ifNavigated()
            val intent: Intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q=${place.lat},${place.long}&mode=d")
            )
            intent.setPackage("com.google.android.apps.maps")
            startActivity(intent)
            dialog.cancel()
        }
        dialog.show()
    }


    private fun ifNavigated(){

            val userInfo = bundleOf(
                "uId" to uid,
                "placeName" to placeName,
                "placePic" to placePic
            )
            findNavController().navigate(R.id.action_googleMapFragment_to_raiting,userInfo)
    }
}