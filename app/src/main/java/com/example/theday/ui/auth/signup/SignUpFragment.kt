package com.example.theday.ui.auth.signup

import android.app.Dialog
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.example.theday.BaseFragment
import com.example.theday.R
import com.example.theday.databinding.SignUpFragmentBinding
import com.example.theday.extencions.isEmail
import com.example.theday.extencions.setUp
import com.example.theday.model.UserInfo
import com.example.theday.network.Resource

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : BaseFragment<SignUpFragmentBinding, SignUpViewModel>(
    SignUpFragmentBinding::inflate,
    SignUpViewModel::class.java
) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        listeners()
        observe()
    }


    private fun listeners() {

        binding!!.registerBtn.setOnClickListener {

            signUp()

        }

        binding!!.alreadyRegisteredBtn.setOnClickListener {

            findNavController().navigateUp()

        }
    }

    private fun signUp() {

        val email = binding!!.emailET.text.toString().trim()
        val password = binding!!.passwordET.text.toString().trim()
        val firstName = binding!!.firstNameET.text.toString().trim()
        val lastName = binding!!.lastNameET.text.toString().trim()
        val userName = binding!!.userName.text.toString().trim()

        if (email.isEmpty()) {

            showErrorDialog("Something went wrong", "Email is required")
            return

        }

        if (!email.isEmail()) {

            showErrorDialog("Something went wrong", "Email is incorrect")
            return

        }

        if (password.isEmpty()) {
            showErrorDialog("Something went wrong", "Password is required")
            return

        }

        if (firstName.isEmpty()) {
            showErrorDialog("Something went wrong", "FirstName is required")
            return

        }

        if (lastName.isEmpty()) {
            showErrorDialog("Something went wrong", "LastName is required")
            return

        }

        if (userName.isEmpty()) {
            showErrorDialog("Something went wrong", "UserName is required")
            return

        }

        binding!!.progressBarr.isVisible = true
        viewModel.signUp(email, password)

    }

    private fun observe() {

        viewModel._signUpLiveData.observe(viewLifecycleOwner, {
            binding!!.progressBarr.isVisible = false
            when (it.status) {

                Resource.Status.Succsess -> {

                    val userInfo = UserInfo(
                        binding!!.emailET.text.toString().trim(),
                        binding!!.firstNameET.text.toString().trim(),
                        binding!!.lastNameET.text.toString().trim(),
                        binding!!.userName.text.toString().trim()
                    )
                    viewModel.saveUserInfo(it.data!!.localId!!, userInfo)

                    findNavController().navigateUp()

                }
                Resource.Status.Error -> {
                    showErrorDialog("Something went wrong", it.message.toString())
                }

                else -> {
                }
            }
        })
    }

    private fun showErrorDialog(title: String, description: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description
        dialog.findViewById<Button>(R.id.continuee).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }
}