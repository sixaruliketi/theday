package com.example.theday.ui.questionnaire

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.theday.R
import com.example.theday.databinding.QuestionsItemBinding
import com.example.theday.model.Questionnaire

typealias selectedItem = (position: Boolean) -> Unit

class QuestionnaireRecyclerViewAdapter :
    RecyclerView.Adapter<QuestionnaireRecyclerViewAdapter.ItemViewHolder>() {

    lateinit var selectedItem: selectedItem
    var selected: Int = 0

    var items = mutableListOf<Questionnaire>()
    var grade: Int = 0
    var counter: Int = 0

    inner class ItemViewHolder(private val binding: QuestionsItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var model: Questionnaire

        fun bind() {
            model = items[absoluteAdapterPosition]

            binding.question.text = model.question
            binding.answerText1.text = model.answerTxt1
            binding.answerText2.text = model.answerTxt2
            binding.answerText3.text = model.answerTxt3
            binding.answerText4.text = model.answerTxt4

            binding.answerText1.setOnClickListener {
                it.setBackgroundResource(R.drawable.selected_answer_bg)
                grade += 1
                counter += 1
                disableBtns()
            }
            binding.answerText2.setOnClickListener {
                it.setBackgroundResource(R.drawable.selected_answer_bg)
                grade += 2
                counter += 1
                disableBtns()
            }
            binding.answerText3.setOnClickListener {
                it.setBackgroundResource(R.drawable.selected_answer_bg)
                grade += 3
                counter += 1
                disableBtns()
            }
            binding.answerText4.setOnClickListener {
                it.setBackgroundResource(R.drawable.selected_answer_bg)
                grade += 4
                counter += 1
                disableBtns()
            }

            if (absoluteAdapterPosition == items.size - 1) {
                binding.doneButton.isVisible = true
            }

            binding.doneButton.setOnClickListener(this)
        }

        fun disableBtns() {

            binding.answerText2.isClickable = false
            binding.answerText3.isClickable = false
            binding.answerText4.isClickable = false
            binding.answerText1.isClickable = false

        }

        override fun onClick(v: View?) {
            selectedItem.invoke(true)
            selected = absoluteAdapterPosition
            notifyDataSetChanged()
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ItemViewHolder(
        QuestionsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    fun getUserGrade(): Int {
        return grade
    }

    override fun getItemCount() = items.size

    fun getData(questionnaire: List<Questionnaire>) {
        items.clear()
        this.items = questionnaire as MutableList<Questionnaire>
        notifyDataSetChanged()
    }
}