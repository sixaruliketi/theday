package com.example.theday.ui.googlemap

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.theday.model.Place
import com.example.theday.network.Resource
import com.example.theday.repository.GoogleMapRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class GoogleMapViewModel @Inject constructor(private val googleMapRepo : GoogleMapRepository) : ViewModel() {

    private var placeLivedata = MutableLiveData<Resource<Place>>()

    var _placeLivedata: LiveData<Resource<Place>> = placeLivedata


    fun getPlace(placeName: String) {


        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                googleMapRepo.getPlace(placeName){
                    placeLivedata.postValue(it)
                }
            }
        }
    }

    fun ready(placeName: String, cout: Int) {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                googleMapRepo.ready(placeName,cout)
            }
        }
    }

    fun navigated(uId: String) {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                googleMapRepo.navigated(uId)
            }
        }
    }


    fun activeEvent(uId: String) {

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                googleMapRepo.activeEvent(uId)
            }
        }
    }

    fun setEventName(uId: String, eventName: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                googleMapRepo.setEventName(uId,eventName)
            }
        }
    }
}