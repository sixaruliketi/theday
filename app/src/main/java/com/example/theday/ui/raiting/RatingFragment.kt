package com.example.theday.ui.raiting

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.theday.BaseFragment
import com.example.theday.R
import com.example.theday.databinding.RaitingFragmentBinding
import com.example.theday.model.UserPlaces
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RatingFragment : BaseFragment<RaitingFragmentBinding, RatingViewModel>(
    RaitingFragmentBinding::inflate,
    RatingViewModel::class.java
) {

    private var uid = ""
    private var placeName = ""
    private var placePic = " "


    override fun start(inflater: LayoutInflater, container: ViewGroup?) {

        uid = arguments?.getString("uId").toString()
        placeName = arguments?.getString("placeName").toString()
        placePic = arguments?.getString("placePic").toString()

        init()

    }

    private fun init() {

        listeners()
        updateInfo()

    }

    private fun listeners() {

        binding!!.donteBtn.setOnClickListener {
            binding!!.progressBar.visibility = View.VISIBLE
            findNavController().navigate(
                R.id.action_raiting_to_homeFragment, bundleOf("uId" to uid,)
            )

        }

    }

    private fun updateInfo() {

        viewModel.addUserHistory(uid, placeName, UserPlaces(placeName, placePic, 3.5))
        viewModel.deactivateEvent(uid)
        viewModel.deactivateNavigate(uid)

    }


}