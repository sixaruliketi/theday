package com.example.theday.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.theday.model.UserInfo
import com.example.theday.model.UserPlaces
import com.example.theday.network.Resource
import com.example.theday.repository.HomeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val homeRepo : HomeRepository) : ViewModel() {

    private var userInfo = MutableLiveData<Resource<UserInfo>>()

    var _userInfo: LiveData<Resource<UserInfo>> = userInfo


    private var userPlacesLivedata = MutableLiveData<Resource<MutableList<UserPlaces>>>()

    var _userPlacesLivedata: LiveData<Resource<MutableList<UserPlaces>>> = userPlacesLivedata


    fun getUserInfo (uid:String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                homeRepo.getUserInfo(uid){
                    userInfo.postValue(it)
                }

            }

        }


    }



    fun getUserPlaceHistory(uid:String){

        viewModelScope.launch {
            withContext(Dispatchers.IO){
                homeRepo.getUserPlaceHistory(uid){

                    userPlacesLivedata.postValue(it)

                }

            }

        }

    }




}

