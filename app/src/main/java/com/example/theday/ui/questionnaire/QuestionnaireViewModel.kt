package com.example.theday.ui.questionnaire

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.theday.model.Questionnaire
import com.example.theday.network.Resource
import com.example.theday.repository.QuestionnaireRepository
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class QuestionnaireViewModel @Inject constructor(private val questionnaireRepo : QuestionnaireRepository): ViewModel() {

    private var getQuestionnaireLivedata = MutableLiveData<Resource<MutableList<Questionnaire>>>()

    var _getQuestionnaireLivedata: LiveData<Resource<MutableList<Questionnaire>>> = getQuestionnaireLivedata


    fun getItem() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                questionnaireRepo.getItem {
                    getQuestionnaireLivedata.postValue(it)
                }
            }
        }
    }
}