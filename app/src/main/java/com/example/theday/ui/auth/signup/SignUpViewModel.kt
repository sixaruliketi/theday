package com.example.theday.ui.auth.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.theday.model.SignUp
import com.example.theday.model.UserInfo
import com.example.theday.network.Resource
import com.example.theday.repository.AuthRepository
import com.google.firebase.database.FirebaseDatabase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(private val authRepository: AuthRepository) :
    ViewModel() {

    private var signUpLiveData = MutableLiveData<Resource<SignUp>>()

    var _signUpLiveData: LiveData<Resource<SignUp>> = signUpLiveData

    fun signUp(email: String, password: String) {


        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val result = authRepository.signUp(email, password)
                signUpLiveData.postValue(result)
            }
        }
    }


    fun saveUserInfo(userId: String, user: UserInfo) {
        FirebaseDatabase.getInstance().getReference("User").child(userId).setValue(user)
    }
}