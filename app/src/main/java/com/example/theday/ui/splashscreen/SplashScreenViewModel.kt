package com.example.theday.ui.splashscreen

import android.content.Context
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

@HiltViewModel
class SplashScreenViewModel @Inject constructor(@ApplicationContext context: Context) :
    ViewModel() {


    val sharedPreference = context.getSharedPreferences("user", Context.MODE_PRIVATE)

    fun navigateOnIntro() = sharedPreference.getBoolean("INTRO", false)


}