package com.example.theday.ui.raiting

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.theday.model.UserPlaces
import com.example.theday.repository.RatingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class RatingViewModel @Inject constructor(private val raitingRepo : RatingRepository) : ViewModel() {



     fun addUserHistory(uid:String,placeName:String,place:UserPlaces){
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                raitingRepo.addUserHistory(uid,placeName,place)
            }
        }
    }


    fun deactivateEvent(uid:String){

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                raitingRepo.deactivateEvent(uid)
            }
        }
    }

    fun deactivateNavigate(uid:String){

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                raitingRepo.deactivateNavigate(uid)
            }
        }

    }
}