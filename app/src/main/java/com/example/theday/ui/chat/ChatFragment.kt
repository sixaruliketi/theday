package com.example.theday.ui.chat

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.theday.R
import com.example.theday.databinding.ChatFragmentBinding
import com.example.theday.extencions.setUp
import com.example.theday.model.Message
import com.example.theday.network.Resource
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChatFragment : BottomSheetDialogFragment() {

    private var placeName = ""
    private var uid = ""
    private var userName = ""
    private var userPic = ""

    private lateinit var binding: ChatFragmentBinding

    private val viewModel: ChatViewModel by viewModels()

    private lateinit var adapter: ChatRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = ChatFragmentBinding.inflate(inflater, container, false)

        placeName = arguments?.getString("placeName").toString()
        uid = arguments?.getString("uid").toString()
        userName = arguments?.getString("userName").toString()
        userPic = arguments?.getString("userPic").toString()

        init()
        return binding.root

    }

    private fun init() {

        initRecycler()
        observe()
        listeners()
        viewModel.getMessages(placeName)

    }

    private fun initRecycler() {

        adapter = ChatRecyclerAdapter(uid)
        binding.chatRecycler.layoutManager = LinearLayoutManager(context)
        binding.chatRecycler.adapter = adapter


    }

    private fun listeners() {

        binding.sendBtn.setOnClickListener {
            sendMessage()
            binding.messageET.setText("")
        }

    }

    private fun sendMessage() {

        val message = binding.messageET.text.toString()

        if (message.isNotEmpty()) {
            viewModel.sentMessage(placeName, Message(message, uid, userName, userPic))
        } else {
            return
        }

    }

    private fun observe() {

        viewModel._getMessagesLivedata.observe(viewLifecycleOwner, {

            when(it.status){

                Resource.Status.Succsess ->{
                    adapter.getMessages(it.data!!)
                    binding.chatRecycler.scrollToPosition(it.data.size - 1)
                }

                Resource.Status.Error ->{
                    showErrorDialog("Something went wrong",it.message.toString())
                }
                else->{}

            }

        })

    }

    private fun showErrorDialog(title: String, description: String) {
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog)
        dialog.findViewById<TextView>(R.id.dialogTitle).text = title
        dialog.findViewById<TextView>(R.id.dialogDescription).text = description
        dialog.findViewById<Button>(R.id.continuee).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }
}