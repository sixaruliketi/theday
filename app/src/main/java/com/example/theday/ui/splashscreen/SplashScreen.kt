package com.example.theday.ui.splashscreen

import android.os.Handler
import android.os.Looper.getMainLooper
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.theday.BaseFragment
import com.example.theday.R
import com.example.theday.databinding.SplashScreenFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashScreen : BaseFragment<SplashScreenFragmentBinding, SplashScreenViewModel>(
    SplashScreenFragmentBinding::inflate,
    SplashScreenViewModel::class.java
) {


    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        navigation()
    }


    fun navigation(){
        if (!viewModel.navigateOnIntro()) {
            Handler(getMainLooper()).postDelayed({
                findNavController().navigate(R.id.action_splashScreen_to_introFragment)
            }, 3000)

        }else{
            Handler(getMainLooper()).postDelayed({
                findNavController().navigate(R.id.action_splashScreen_to_logInFragment)
            }, 3000)
        }
    }

}
